﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CuriousNumbers.Tests
{
    [TestClass]
    public class FactiorialEngineTests
    {
        [TestMethod]
        public void CountTestWithZero()
        {
            var _temp = new FactiorialEngine();

            int factorial = _temp.count(0);

            Assert.AreEqual(1, factorial);
        }


        [TestMethod]
        public void CountTestWithPositiveNumber()
        {
            var _temp = new FactiorialEngine();

            int factorial = _temp.count(10);

            Assert.AreEqual(3628800, factorial);
        }
    }
}
