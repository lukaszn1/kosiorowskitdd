﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Numerics;

namespace CuriousNumbers.Tests
{
    [TestClass]
    public class NumberParserTests
    {
        [TestMethod]
        public void ParseNumberToDigitsTestWithZero()
        {
            var _temp = new NumberParser();
            List<BigInteger> wzorzec = new List<BigInteger> { };

            List<BigInteger> lista = _temp.parseNumberToDigits(0);

            CollectionAssert.AreEqual(wzorzec, lista);
        }

        [TestMethod]
        public void ParseNumberToDigitsTestWithPositiveNumber()
        {
            var _temp = new NumberParser();
            List<BigInteger> wzorzec = new List<BigInteger> { 3,2,1 };

            List<BigInteger> lista = _temp.parseNumberToDigits(123);

            CollectionAssert.AreEqual(wzorzec, lista);
        }

        [TestMethod]
        public void ParseNumberToDigitsTestWithBigPositiveNumber()
        {
            var _temp = new NumberParser();
            List<BigInteger> wzorzec = new List<BigInteger> { 9,8,7,6,5,4,3,2,1};

            List<BigInteger> lista = _temp.parseNumberToDigits(123456789);

            CollectionAssert.AreEqual(wzorzec, lista);
        }
    }
}
