﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Numerics;

namespace CuriousNumbers.Tests
{
    [TestClass]
    public class CuriousNumbersFinderTests
    {
        [TestMethod]
        public void findCuriousNumbersPositiveNumber()
        {
            var _temp = new CuriousNumbersFinder();
            var wzorzec = new HashSet<BigInteger> { 145 };

            var lista = _temp.findCuriousNumbers(200);

            CollectionAssert.AreEqual(new List<BigInteger>(wzorzec), new List<BigInteger>(lista));
        }

        [TestMethod]
        public void findCuriousNumbersBigPositiveNumber()
        {
            var _temp = new CuriousNumbersFinder();
            var wzorzec = new HashSet<BigInteger> { 145, 40585 };

            var lista = _temp.findCuriousNumbers(45000);

            CollectionAssert.AreEqual(new List<BigInteger>(wzorzec), new List<BigInteger>(lista));
        }
    }
}
