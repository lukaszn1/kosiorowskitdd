﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CuriousNumbers
{
    public class CuriousNumbersFinder
    {
        private FactiorialEngine factiorial;
        private NumberParser numberParser;

        public CuriousNumbersFinder()
        {
            factiorial = new FactiorialEngine();
            numberParser = new NumberParser();
        }

        /**
         * @example:
         * findCuriousNumber(200) -> 145
         * findCuriousNumber(45000) -> 145, 40585
         *
         * @param maxNumberToCheck max numbers we want to check (loop limit)
         * @return numbers who has then factorial digits sum is equal to this number
         */
        public HashSet<BigInteger> findCuriousNumbers(int maxNumberToCheck)
        {
            HashSet<BigInteger> curiousNumbers = new HashSet<BigInteger>();
            BigInteger firstNumberToCheck = 10;
            BigInteger actualNumber = firstNumberToCheck;
           
            for (int i = 0; i < maxNumberToCheck; i++)
            {
                int sumOfParsed = 0;
                List<BigInteger> parsedNumbers = numberParser.parseNumberToDigits(actualNumber);

                foreach (BigInteger parsedNumber in parsedNumbers)
                {
                    sumOfParsed += factiorial.count(parseBigIntegerToInt(parsedNumber));
                }

                if (isNumbersAreEqual(actualNumber, sumOfParsed))
                {
                    curiousNumbers.Add(actualNumber);
                }

                actualNumber++;
            }

            return curiousNumbers;
        }

        private int parseBigIntegerToInt(BigInteger parsedNumber)
        {
            try
            {
                return (int)parsedNumber;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Nie mozna wykonac rzutowania");
                Console.WriteLine(ex);
                return -1;
            }
        }

        private Boolean isNumbersAreEqual(BigInteger actualNumber, int sumOfParsed)
        {
            return actualNumber == sumOfParsed;
        }
    }

}
