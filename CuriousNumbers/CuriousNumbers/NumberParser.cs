﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CuriousNumbers
{
    public class NumberParser
    {
        private BigInteger removeLastDigitFromNumber(BigInteger number)
        {
            return BigInteger.Divide(number, 10);
        }

        private void addDigitToGlobalCollection(BigInteger digit, List<BigInteger> collection)
        {
            collection.Add(BigInteger.ModPow(digit, 1, 10));
        }

        private Boolean hasMoreDigits(BigInteger toParse)
        {
            return toParse != 0;
        }

        /**
         * @example:
         * parseNumberToDigits(5342) -> 5,3,4,2
         * parseNumberToDigits(15987) -> 1,5,9,8,7
         *
         * @param toParse - number what we want to parse to digits list
         * @return digit list of given Number
         */
        public List<BigInteger> parseNumberToDigits(BigInteger toParse)
        {
            List<BigInteger> parsedNumbers = new List<BigInteger>();

            while (hasMoreDigits(toParse))
            {
                addDigitToGlobalCollection(toParse, parsedNumbers);
                toParse = removeLastDigitFromNumber(toParse);
            }
            return parsedNumbers;
        }
    }
}