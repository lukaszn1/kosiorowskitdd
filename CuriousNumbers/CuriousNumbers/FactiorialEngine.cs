﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuriousNumbers
{
    public class FactiorialEngine
    {
        public int count(int n)
        {
            if (n < 0)
                throw new ArgumentException("n mustn't be negative.");

            int factorial = 1;
            for (int i = 2; i <= n; i++)
            {
                factorial = factorial * i;
            }

            return factorial;
        }
    }
}
