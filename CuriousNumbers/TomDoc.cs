# Public: Wylicza silnie dla n
#
# n - liczba całkowita z zakresu [-2^31, 2^31-1] 
#
# Examples
#
#   count(4)
#   # => 24
#
# Zwraca silnię dla podanego arguentu
public int count(int n)
{
	if (n < 0)
		throw new ArgumentException("n mustn't be negative.");

	int factorial = 1;
	for (int i = 2; i <= n; i++)
	{
		factorial = factorial * i;
	}

	return factorial;
}